# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include vlc::install
class vlc::install {
  package { $vlc::package_name:
    ensure => $vlc::package_ensure,
  }
}
